package utils;

import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import com.mongodb.util.JSON;
import mongo.MongoManager;

/**
 * Created by vinoth on 7/8/16.
 */
public class MongoUtils {
    public static DBObject getDbObject(String message){
        return (DBObject) JSON.parse(message);
    }

    public static BasicDBObject getBasicUniqueKeyObject(DBObject object){
        BasicDBObject document = new BasicDBObject();
        document.put("chain", object.get("chain"));
        document.put("store", object.get("store"));
        return document;
    }

    public static BasicDBObject getDeviceQuery(DBObject object){
        BasicDBObject document = new BasicDBObject();
        document.put("chain", object.get("chain"));
        document.put("store", object.get("store"));
        document.put("host_id", object.get("host_id"));
        return document;
    }

    public static void update(String tableName, BasicDBObject query, DBObject set, boolean isUpsert){
        MongoManager.getDb().getCollection(tableName).update(query, set, isUpsert, false);
    }

    public static void insert(String tableName, DBObject set){
        MongoManager.getDb().getCollection(tableName).insert(set);
    }

    public static DBObject findOne(String tableName, BasicDBObject query){
        return MongoManager.getDb().getCollection(tableName).findOne(query);
    }

    // MongoManager.getDb().getCollection(QueueNameConstants.DEVICE).update(document, dbObject,true,false);

}
