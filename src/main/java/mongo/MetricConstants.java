package mongo;

import constants.QueueNameConstants;

/**
 * Created by vinoth on 5/8/16.
 */
public class MetricConstants extends QueueNameConstants{
    public static String[] METRIC_TABLES = {DEVICE, ORATION_CREATION,
            ORATION_LISTENED, MOS, PACKET_LOSS_SERVER,
            TAG_HEART_BEAT, STATIC_ANNOUNCEMENT, ALARMS, TGS_STATE, SUPPORT_COMMANDS, DEVICE_COUNT};

}
