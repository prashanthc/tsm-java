package mongo;

/**
 * Created by vinoth on 5/8/16.
 */

import com.mongodb.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import utils.PropertyUtils;

import java.io.IOException;
import java.net.InetAddress;

//TODO : make it singleton , refer RMQCOnnectionFactory
public class MongoManager {
    private static DB db = null;

    private static Logger log = LoggerFactory.getLogger(MongoManager.class.getName());
    private static final int DB_CONNECTION_MAX_TRY_COUNTER = 6;
    private static final int DB_CONNECTION_RETRY_WAIT_TIME = 10000;
    private static final String DB_CONNECTION_REQUEST_INFO_MESSAGE_TEXT = "Trying to establish DB connection";
    private static final String DB_CONNECTION_FAILED_ERROR_MESSAGE_TEXT = "DB connection failed";
    private static final String DB_CONNECTION_RETRY_INFO_MESSAGE_TEXT = "Retrying to establish DB connection";
    private static final String SYSTEM_EXIT_ERROR_MESSAGE_TEXT = "To is unable to boot up after installation";

/*
	private MongoManager() throws MongoException, IOException {
		initMongo();
	}*/

    /**
     * Initialize Mongo DB
     *
     * @throws MongoException
     * @throws IOException
     */
    public void initMongo() throws MongoException, IOException {

        int noSqlPort = 27017;

        String tomAddress = PropertyUtils.getInstance().getMongoProperty("mongo.host");
        log.info("Address [ {} ]", tomAddress);

        InetAddress inetAddress = InetAddress.getByName(tomAddress);
        log.debug("Server Name [ {} ]", inetAddress.getHostName());

        ServerAddress serverAddress = new ServerAddress(inetAddress, noSqlPort);

        Mongo mongo = new MongoClient(serverAddress);
        log.info("Mongo URL [ {} ]", mongo.getAddress());

        int connectionTryCounter = DB_CONNECTION_MAX_TRY_COUNTER;
        log.info(DB_CONNECTION_REQUEST_INFO_MESSAGE_TEXT);
        while (connectionTryCounter > 0){
            try{
                // Mongo DB
                db = mongo.getDB(PropertyUtils.getInstance().getMongoProperty("mongo.db"));

                // Create tables if not exist
                initTables();
                connectionTryCounter = 0;
            }catch(MongoException mongoException){
                log.error(DB_CONNECTION_FAILED_ERROR_MESSAGE_TEXT);
                connectionTryCounter--;
                if ( connectionTryCounter == 0){
                    log.error(mongoException.getMessage());
                    mongoException.printStackTrace();
                    log.error(SYSTEM_EXIT_ERROR_MESSAGE_TEXT);
                    System.exit(1); // Since failed to connect DB, trigger system exit .
                }else{
                    try {
                        Thread.sleep(DB_CONNECTION_RETRY_WAIT_TIME); //Sleep for 10 seconds and retry again.
                    } catch (InterruptedException interruptedException) {
                        interruptedException.printStackTrace();
                    }
                    log.info(DB_CONNECTION_RETRY_INFO_MESSAGE_TEXT);
                }
            }
        }
    }

    private void initTables() {
        // Create metric tables if not exist
        for (String metricTable : MetricConstants.METRIC_TABLES) {
            if (!db.collectionExists(metricTable)) {
                db.createCollection(metricTable, new BasicDBObject());
            }
        }
    }

    public static DB getDb() {
        return db;
    }
}
