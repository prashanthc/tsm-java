package consumers;

import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import com.rabbitmq.client.Connection;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import utils.MongoUtils;

/**
 * Created by vinoth on 8/8/16.
 */
public class SupportCommandsConsumer extends RMQConsumerMessage{
    private Connection connection;
    private static final Logger LOGGER = LoggerFactory.getLogger(SupportCommandsConsumer.class);

    public SupportCommandsConsumer(Connection connection){
        this.connection = connection;
    }

    @Override
    public void consumeMessage(String queueName, String bindingKey, boolean autoAck) {
        consume(this, connection, bindingKey, autoAck);

    }

    @Override
    public void processMsgData(String message) {
        LOGGER.info("Support commands message : {}", message);
        DBObject set = MongoUtils.getDbObject(message);
        BasicDBObject query = MongoUtils.getBasicUniqueKeyObject(set);

        //MongoUtils.update(QueueNameConstants.SUPPORT_COMMANDS, query, set, true);
    }
}
