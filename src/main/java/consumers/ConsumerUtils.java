package consumers;

import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.InetAddress;

/**
 * Created by vinoth on 4/8/16.
 */
public class ConsumerUtils {
    private static final Logger LOGGER = LoggerFactory.getLogger(ConsumerUtils.class);

    public static String cleanMessage(String message){
        String cleanedMessage = message;
        cleanedMessage = message.replaceAll("True","true").replaceAll("False", "false").replaceAll("remoteTxId", "txId");
        return cleanedMessage;
    }

    public static String getHostName() {
        String hostName = "dummyHostName";
        try {
            hostName = InetAddress.getLocalHost().getHostName();
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }
        return hostName;
    }

    public static final String getDate(Double time) {
        Long ll = Math.round(time);
        if(Long.toString(ll).length()>10){
            String newTime = Long.toString(ll).substring(0, 10);
            ll = Long.parseLong(newTime);
        }
        long lTime = ll * 1000;
        final DateTime dt = new DateTime(lTime);
        return dt.getDayOfMonth()+"/"+dt.getMonthOfYear()+"/"+dt.getYear();

    }

}
