package consumers;

import com.rabbitmq.client.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import utils.PropertyUtils;

import java.io.IOException;

/**
 * Created by vinoth on 7/8/16.
 */

//TODO : Check this implementation
public abstract class RMQConsumerMessage implements RMQConsumer {
    private Channel channel;
    private static final Logger LOGGER = LoggerFactory.getLogger(RMQConsumerMessage.class);

    public void consume(RMQConsumer consumerObject, Connection connection, String bindingKey,boolean autoAck){
        try {
            channel = connection.createChannel();
            String queueNameWithHost = ConsumerUtils.getHostName() + ":" + bindingKey;
            channel.exchangeDeclare(PropertyUtils.getInstance().getRMQProperty("Exchange_name"), RMQConstants.TOPIC_EXCHANGE);
            channel.queueDeclare(queueNameWithHost, false, false, false, null);
            channel.queueBind(queueNameWithHost, PropertyUtils.getInstance().getRMQProperty("Exchange_name"), bindingKey);

            Consumer consumer = new DefaultConsumer(channel){
                @Override
                public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body) throws IOException {
                    String message = new String(body);
                    message=ConsumerUtils.cleanMessage(message);
                    consumerObject.processMsgData(message);
                    if(!autoAck){
                        channel.basicAck(envelope.getDeliveryTag(),RMQConstants.BASIC_ACK_ALL);
                    }
                }
            };
            channel.basicConsume(queueNameWithHost, autoAck, consumer);
        } catch (IOException e) {
            LOGGER.error("IO Exception "+e);
        }
    }
}