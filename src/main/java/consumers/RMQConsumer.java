package consumers;

/**
 * Created by vinoth on 4/8/16.
 */
public interface RMQConsumer {
    void consumeMessage(String queueName,String bindingKey,boolean autoAck);
    void processMsgData(String message);

}
