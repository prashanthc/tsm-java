package consumers;

import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import com.rabbitmq.client.Connection;
import constants.CommonFields;
import constants.QueueNameConstants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pojo.DeviceCountBasicDb;
import utils.MongoUtils;

/**
 * Created by vinoth on 4/8/16.
 */

//TODO : Add LOGS
public class DeviceConsumer extends RMQConsumerMessage {
    private Connection connection;
    private static final Logger LOGGER = LoggerFactory.getLogger(DeviceConsumer.class);

    public DeviceConsumer(Connection connection) {
        this.connection = connection;
    }
    @Override
    public void consumeMessage(String queueName, String bindingKey, boolean autoAck) {
        consume(this, connection, bindingKey, autoAck);
    }

    @Override
    //TODO: seprate Unique keys for every collection
    public void processMsgData(String message) {
        LOGGER.info("Incoming device message : {}", message);
        if(isRequiredFieldsAvilable(message)){
            DBObject newDbObject = MongoUtils.getDbObject(message);
            BasicDBObject query = MongoUtils.getBasicUniqueKeyObject(newDbObject);
            query.put("host_id", newDbObject.get("host_id"));

            DBObject oldDbObject = InMemoryData.getInstance().getDbObject(newDbObject.get(CommonFields.HOST_ID).toString());
            DeviceCountBasicDb updateDeviceCount = toUpdateDeviceCount(newDbObject, oldDbObject);

            if (updateDeviceCount != null) {
                if (updateDeviceCount.getDeviceCountUpdate() != null) {
                    //TODO : query2 rename
                    BasicDBObject query2 = MongoUtils.getBasicUniqueKeyObject(newDbObject);
                    DBObject deviceCountMongoObj = MongoUtils.findOne(QueueNameConstants.DEVICE_COUNT, query2);
                    if (deviceCountMongoObj != null) {
                        MongoUtils.update(QueueNameConstants.DEVICE_COUNT, query2, updateDeviceCount.getDeviceCountUpdate(), true);
                        LOGGER.info("Updating device count");
                    } else {
                        MongoUtils.insert(QueueNameConstants.DEVICE_COUNT, insertDeviceCount(newDbObject));
                        LOGGER.info("Inserting device count entry");
                    }
                }
                if (updateDeviceCount.getDeviceUpdate() != null) {
                    LOGGER.info("Updating Device");
                    MongoUtils.update(QueueNameConstants.DEVICE, query, newDbObject, true);
                    MongoUtils.update(QueueNameConstants.DEVICE, query, updateDeviceCount.getDeviceUpdate(), true);
                } else {
                    MongoUtils.update(QueueNameConstants.DEVICE, query, newDbObject, true);
                }
            } else {
                MongoUtils.update(QueueNameConstants.DEVICE, query, newDbObject, true);
            }

            InMemoryData.getInstance().addOrUpdateDevice(newDbObject);
            LOGGER.info("In memory size :: {}", InMemoryData.getInstance().getInMemoryDeviceSize());
        }else{
            LOGGER.warn("Not consuming ");
        }

    }

    private boolean isRequiredFieldsAvilable(String message) {
        return message.contains(CommonFields.CHAIN) && message.contains(CommonFields.STORE) &&
                message.contains(CommonFields.HOST_ID) && message.contains(CommonFields.LOST_PLAY_CREDIT_TIME) &&
                message.contains(CommonFields.MSG_TYPE) && message.contains(CommonFields.OFFLINE_GT_DAY);
    }

    private DBObject insertDeviceCount(DBObject newDbObject) {
        BasicDBObject basicDBObject = MongoUtils.getBasicUniqueKeyObject(newDbObject);
        basicDBObject.put("count",0);
        if(newDbObject.get(CommonFields.LOST_PLAY_CREDIT_TIME) != null){
            basicDBObject.put("offline", 1);
        } else {
            basicDBObject.put("offline", 0);
        }

        if(newDbObject.get(CommonFields.OFFLINE_GT_DAY)!=null &&
                Boolean.parseBoolean(newDbObject.get(CommonFields.OFFLINE_GT_DAY).toString())){
            basicDBObject.put("offline_gt_day", 1);
        }else {
            basicDBObject.put("offline_gt_day", 0);
        }

        return basicDBObject;

    }

    private DeviceCountBasicDb toUpdateDeviceCount(DBObject newDbObject, DBObject oldDbObject) {
        String newLostPlayCredit = null;
        if(newDbObject.get(CommonFields.LOST_PLAY_CREDIT_TIME) != null){
            newLostPlayCredit = newDbObject.get(CommonFields.LOST_PLAY_CREDIT_TIME).toString();
        }

        String newOfflineGTDay = newDbObject.get(CommonFields.OFFLINE_GT_DAY).toString();

        String oldLostPlayCredit = null;
        String oldOfflineGTDay = null;
        String oldHostId = null;


        DeviceCountBasicDb deviceCount = null;

        if(oldDbObject != null){
            oldHostId = oldDbObject.get(CommonFields.HOST_ID).toString();
            if(oldDbObject.get(CommonFields.LOST_PLAY_CREDIT_TIME) != null) {
                oldLostPlayCredit = oldDbObject.get(CommonFields.LOST_PLAY_CREDIT_TIME).toString();
            }
            oldOfflineGTDay = oldDbObject.get(CommonFields.OFFLINE_GT_DAY).toString();
        }else {
            //taking from mongo db
            DBObject oldMongoDbObject = MongoUtils.findOne(QueueNameConstants.DEVICE, MongoUtils.getDeviceQuery(newDbObject));
            if(oldMongoDbObject != null) {
                oldHostId = oldMongoDbObject.get(CommonFields.HOST_ID).toString();
                if(oldMongoDbObject.get(CommonFields.LOST_PLAY_CREDIT_TIME)!= null)
                    oldLostPlayCredit = oldMongoDbObject.get(CommonFields.LOST_PLAY_CREDIT_TIME).toString();

                oldOfflineGTDay = oldMongoDbObject.get(CommonFields.OFFLINE_GT_DAY).toString();
            }
        }

        if(oldHostId != null && !oldHostId.isEmpty()){
            deviceCount = getDeviceCountUpdateData(newLostPlayCredit, newOfflineGTDay, oldLostPlayCredit, oldOfflineGTDay);

        }else{
            deviceCount = getFirstEntryDeviceCountData(newDbObject, newLostPlayCredit, newOfflineGTDay);
        }

        return deviceCount;
    }

    private DeviceCountBasicDb getDeviceCountUpdateData(String newLostPlayCredit, String newOfflineGTDay, String oldLostPlayCredit, String oldOfflineGTDay) {

        DeviceCountBasicDb deviceCountBasicDb = new DeviceCountBasicDb();
        //TODO : Rename variable to: offlineCountUpdateBy
        int offlineCount=0;
        int offlineGreaterThan = 0;

        if(newLostPlayCredit != null && oldLostPlayCredit == null){
            offlineCount = 1;
        }else if(newLostPlayCredit == null && oldLostPlayCredit != null){
            offlineCount = -1;
        }

        if(Boolean.parseBoolean(newOfflineGTDay) && !Boolean.parseBoolean(oldOfflineGTDay)){
            offlineGreaterThan =1;
        }else if(!Boolean.parseBoolean(newOfflineGTDay) && Boolean.parseBoolean(oldOfflineGTDay)){
            offlineGreaterThan = -1;
        }

        deviceCountBasicDb.setDeviceCountUpdate(incrementDeviceCount(offlineCount, offlineGreaterThan));


        if(offlineCount ==1 && offlineCount == 1){
            BasicDBObject device = new BasicDBObject();
            //TODO : Move false to Constants
            device.put("offline_gt_day", false);
            BasicDBObject deviceUpdate = new BasicDBObject("$set", device);
            deviceCountBasicDb.setDeviceUpdate(deviceUpdate);
        }

        return deviceCountBasicDb;
    }

    private DeviceCountBasicDb getFirstEntryDeviceCountData(DBObject newDbObject, String newLostPlayCredit, String newOfflineGTDay) {
        DeviceCountBasicDb deviceCountBasicDb = new DeviceCountBasicDb();
        int offlineCount=0;
        int offlineGreaterThan = 0;


        if(newLostPlayCredit != null){
            offlineCount = 1;
        }
        if(Boolean.parseBoolean(newOfflineGTDay)){
            offlineGreaterThan = 1;
        }
        deviceCountBasicDb.setDeviceCountUpdate(incrementDeviceCount(offlineCount, offlineGreaterThan));

        if(offlineCount == 1 && offlineGreaterThan == 1){
            BasicDBObject device = new BasicDBObject();
            device.put("offline_gt_day", false);
            BasicDBObject deviceUpdate = new BasicDBObject("$set", device);
            deviceCountBasicDb.setDeviceUpdate(deviceUpdate);
        }
        return deviceCountBasicDb;
    }

    private BasicDBObject incrementDeviceCount(int offline, int offlineGreaterThan){
        BasicDBObject device = new BasicDBObject();
        device.put("offline", offline);
        device.put("offline_gt_day", offlineGreaterThan);
        BasicDBObject deviceUpdate = new BasicDBObject("$inc", device);
        return deviceUpdate;
    }
}
