package consumers;

import com.rabbitmq.client.Connection;
import constants.QueueNameConstants;

/**
 * Created by vinoth on 4/8/16.
 */
public class RMQConsumerFactory {
    private static Connection connection;

    public static RMQConsumer getInstance(String queueName){
        connection=RMQConnection.getRmqConnection();
        RMQConsumer rmqConsumer=null;
        if(queueName!=null && ! queueName.isEmpty()){
            switch (queueName){
                case QueueNameConstants.DEVICE :rmqConsumer = new DeviceConsumer(connection);
                    break;
                case QueueNameConstants.ORATION_CREATION :rmqConsumer=new OrationCreationConsumer(connection);
                    break;
                case QueueNameConstants.ORATION_LISTENED :rmqConsumer=new OrationListenConsumer(connection);
                    break;
                case QueueNameConstants.MOS :rmqConsumer=new MosConsumer(connection);
                    break;
                case QueueNameConstants.PACKET_LOSS_SERVER :rmqConsumer=new PacketLossServerConsumer(connection);
                    break;
                case QueueNameConstants.TAG_HEART_BEAT :rmqConsumer=new TagHeartBeatConsumer(connection);
                    break;
                case QueueNameConstants.STATIC_ANNOUNCEMENT :rmqConsumer=new StaticAnnouncementConsumer(connection);
                    break;
                case QueueNameConstants.SUPPORT_COMMANDS :rmqConsumer=new SupportCommandsConsumer(connection);
                    break;
                case QueueNameConstants.TGS_STATE :rmqConsumer=new TgsStateConsumer(connection);
                    break;
                case QueueNameConstants.ALARMS :rmqConsumer=new AlarmsConsumer(connection);
                    break;
            }
        }
        return rmqConsumer;
    }
}
