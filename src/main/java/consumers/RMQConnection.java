package consumers;

import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import utils.PropertyUtils;

import java.io.IOException;

/**
 * Created by vinoth on 4/8/16.
 */
public class RMQConnection {
    public static final Logger LOGGER = LoggerFactory.getLogger(RMQConnection.class);

    private volatile static Connection rmqConnection;

    private RMQConnection(){}

    public static Connection getRmqConnection(){
        if(rmqConnection==null){
            synchronized (RMQConnection.class){
                if(rmqConnection==null){
                    rmqConnection=createRMQConnection();
                }
            }
        }
        return rmqConnection;
    }

    private static Connection createRMQConnection() {
        ConnectionFactory factory = new ConnectionFactory();
        Connection connection=null;
        //ToDo
        factory.setUsername(PropertyUtils.getInstance().getRMQProperty("rmqUserName"));
        factory.setPassword(PropertyUtils.getInstance().getRMQProperty("rmqPassWord"));
        factory.setVirtualHost(PropertyUtils.getInstance().getRMQProperty("rmqVhost"));
        factory.setHost(PropertyUtils.getInstance().getRMQProperty("rmqHost"));
        factory.setPort(5672);
        try {
            connection=factory.newConnection();
        } catch (IOException e) {
            LOGGER.error("{}", e);
        }
        return connection;
    }
}
