package consumers;

import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import com.rabbitmq.client.Connection;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import utils.MongoUtils;

/**
 * Created by vinoth on 7/8/16.
 */
public class OrationListenConsumer extends RMQConsumerMessage  {
    private Connection connection;
    private static final Logger LOGGER = LoggerFactory.getLogger(OrationListenConsumer.class);


    public OrationListenConsumer(Connection connection){
        this.connection = connection;
    }
    @Override
    public void consumeMessage(String queueName, String bindingKey, boolean autoAck) {
        consume(this, connection, bindingKey, autoAck);
    }

    @Override
    public void processMsgData(String message) {
        LOGGER.info("Oration listen message : {}", message);
        DBObject set = MongoUtils.getDbObject(message);
        BasicDBObject query = MongoUtils.getBasicUniqueKeyObject(set);
        query.put("host_id", set.get("host_id"));

        //MongoUtils.update(QueueNameConstants.ORATION_LISTENED, query, set, true);
    }
}
