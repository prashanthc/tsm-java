package consumers;

import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import com.rabbitmq.client.Connection;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import utils.MongoUtils;

/**
 * Created by vinoth on 7/8/16.
 */
public class StaticAnnouncementConsumer extends RMQConsumerMessage  {
    private Connection connection;
    private static final Logger LOGGER = LoggerFactory.getLogger(StaticAnnouncementConsumer.class);


    public StaticAnnouncementConsumer(Connection connection){
        this.connection = connection;
    }
    @Override
    public void consumeMessage(String queueName, String bindingKey, boolean autoAck) {
        consume(this, connection, bindingKey, autoAck);
    }

    @Override
    public void processMsgData(String message) {
        LOGGER.info("Static announcement message : {}", message);
        DBObject set = MongoUtils.getDbObject(message);
        BasicDBObject query = MongoUtils.getBasicUniqueKeyObject(set);

        //MongoUtils.update(QueueNameConstants.STATIC_ANNOUNCEMENT, query, set, true);
    }
}
