package consumers;

import com.mongodb.DBObject;
import constants.QueueNameConstants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import utils.MongoUtils;

import java.util.Map;
import java.util.TimerTask;

/**
 * Created by vinoth on 9/8/16.
 */
    public class ScheduledTask extends TimerTask {
    private final static Logger LOGGER= LoggerFactory.getLogger(ScheduledTask.class);

    @Override
    public void run() {
        Map<String, DBObject> mp = InMemoryData.getDeviceDataMap();
        //LOGGER.debug("Scheduled Task is running");

        for(Object key:mp.keySet()){
            LOGGER.info("Scheduled Task key : {}", key);
            DBObject object = mp.get(key);
            MongoUtils.update(QueueNameConstants.DEVICE, MongoUtils.getDeviceQuery(object), object, true);
        }
    }
}
