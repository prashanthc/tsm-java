package consumers;

import com.rabbitmq.client.Connection;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by vinoth on 7/8/16.
 */
public class MosConsumer extends RMQConsumerMessage  {
    private Connection connection;
    private static final Logger LOGGER = LoggerFactory.getLogger(MosConsumer.class);


    public MosConsumer(Connection connection){
        this.connection = connection;
    }
    @Override
    public void consumeMessage(String queueName, String bindingKey, boolean autoAck) {
        consume(this, connection, bindingKey, autoAck);
    }

    @Override
    public void processMsgData(String message) {
       /* LOGGER.info("Mos Client message : {}", message);
        DBObject data = MongoUtils.getDbObject(message);

        BasicDBObject queryToGet = MongoUtils.getBasicUniqueKeyObject(data);
        queryToGet.put("host_id", data.get("host_id"));

        DBObject deviceData = MongoUtils.findOne(QueueNameConstants.DEVICE, queryToGet);

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("chain", data.get("chain"));
            jsonObject.put("store", data.get("store"));
            jsonObject.put("time", data.get("time"));
            jsonObject.put("date", ConsumerUtils.getDate(Double.parseDouble(data.get("time").toString())));
            jsonObject.put("mos", data.get(""));
            String messageType = data.get("msg_type").toString();

            if(("mos_client").equalsIgnoreCase(messageType)){
                jsonObject.put("source", "client");
                jsonObject.put("oruid", data.get("oru_id"));
            }else if(("mos_server").equalsIgnoreCase(messageType)){
                jsonObject.put("source", "server");
            }

            if(deviceData != null){
                jsonObject.put("AP", deviceData.get("AP"));
                jsonObject.put("board_id", deviceData.get("board_id"));
            }else{
                jsonObject.put("AP", "");
                jsonObject.put("board_id", "");
            }

            jsonObject.put("total", data.get("total"));

            MongoUtils.insert(QueueNameConstants.MOS, MongoUtils.getDbObject(jsonObject.toString()));

        } catch (JSONException e) {
            LOGGER.error("JSON Exception : {}", e);
        }
        */

        //MongoUtils.update(QueueNameConstants.MOS, query, set, true);
    }

}
