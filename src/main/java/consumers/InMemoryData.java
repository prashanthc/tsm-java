package consumers;

import com.mongodb.DBObject;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by vinoth on 9/8/16.
 */

//TODO use COncuurentHashMap
//TODO ; Make this class singleton
public class InMemoryData {
    private static final InMemoryData instance;
    private static final Map<String, DBObject> deviceDataMap;

    static {
        try {
            instance = new InMemoryData();
            deviceDataMap = new ConcurrentHashMap<>();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public static InMemoryData getInstance(){
        return instance;
    }

    public void addOrUpdateDevice(DBObject objectData){
        if(objectData != null && "device".equalsIgnoreCase(objectData.get("msg_type").toString())){
            String key = objectData.get("host_id").toString();
            deviceDataMap.put(key, objectData);
        }
    }

    public DBObject getDbObject(String key){
       return deviceDataMap.get(key);
    }

    public int getInMemoryDeviceSize(){
        return deviceDataMap.size();
    }

    public static Map<String, DBObject> getDeviceDataMap() {
        return deviceDataMap;
    }
}
