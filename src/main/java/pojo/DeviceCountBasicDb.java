package pojo;

import com.mongodb.BasicDBObject;

/**
 * Created by vinoth on 11/8/16.
 */

//TODO : check if you can split device and deviceCount updates to different class
public class DeviceCountBasicDb {
    private BasicDBObject deviceCountUpdate;
    private BasicDBObject deviceUpdate;

    public BasicDBObject getDeviceCountUpdate() {
        return deviceCountUpdate;
    }

    public void setDeviceCountUpdate(BasicDBObject deviceCountUpdate) {
        this.deviceCountUpdate = deviceCountUpdate;
    }

    public BasicDBObject getDeviceUpdate() {
        return deviceUpdate;
    }

    public void setDeviceUpdate(BasicDBObject deviceUpdate) {
        this.deviceUpdate = deviceUpdate;
    }
}
