package pojo;

/**
 * Created by vinoth on 10/8/16.
 */
public class DeviceCount {
    private boolean updateDeviceCount;
    private boolean updateDevice;

    private int lostPlayCredit;
    private int offlineGreaterDay;

    public int getLostPlayCredit() {
        return lostPlayCredit;
    }

    public void setLostPlayCredit(int lostPlayCredit) {
        this.lostPlayCredit = lostPlayCredit;
    }

    public int getOfflineGreaterDay() {
        return offlineGreaterDay;
    }

    public void setOfflineGreaterDay(int offlineGreaterDay) {
        this.offlineGreaterDay = offlineGreaterDay;
    }

    public boolean isUpdateDeviceCount() {
        return updateDeviceCount;
    }

    public void setUpdateDeviceCount(boolean updateDeviceCount) {
        this.updateDeviceCount = updateDeviceCount;
    }

    public boolean isUpdateDevice() {
        return updateDevice;
    }

    public void setUpdateDevice(boolean updateDevice) {
        this.updateDevice = updateDevice;
    }
}
