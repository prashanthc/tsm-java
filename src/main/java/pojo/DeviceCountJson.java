package pojo;

/**
 * Created by vinoth on 10/8/16.
 */
public class DeviceCountJson {
    private String chain;
    private String store;
    private int offline;
    private int offline_gt_day;

    public String getChain() {
        return chain;
    }

    public void setChain(String chain) {
        this.chain = chain;
    }

    public String getStore() {
        return store;
    }

    public void setStore(String store) {
        this.store = store;
    }

    public int getOffline() {
        return offline;
    }

    public void setOffline(int offline) {
        this.offline = offline;
    }

    public int getOffline_gt_day() {
        return offline_gt_day;
    }

    public void setOffline_gt_day(int offline_gt_day) {
        this.offline_gt_day = offline_gt_day;
    }
}
