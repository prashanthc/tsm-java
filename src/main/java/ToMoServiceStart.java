import consumers.ScheduledTask;
import mongo.MongoManager;
import org.apache.log4j.PropertyConfigurator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import rabbitmq.ToMRabbitMqChannelFactory;
import utils.PropertyUtils;

import java.io.FileInputStream;
import java.util.Properties;
import java.util.Timer;

/**
 * Created by vinoth on 4/8/16.
 */
public class ToMoServiceStart {
    private final static Logger LOGGER= LoggerFactory.getLogger(ToMoServiceStart.class);

    public static void main(String[] args) {
        try {
            Properties props = new Properties();
            props.load(new FileInputStream("/opt/theatro/tom/current/resources/properties/log4j.properties"));
            PropertyConfigurator.configure(props);
            ToMRabbitMqChannelFactory.start();
            MongoManager mongoManager = new MongoManager();
            mongoManager.initMongo();
            //TODO : Use TimerTask
            Timer time = new Timer();
            ScheduledTask scheduledTask = new ScheduledTask();
            time.schedule(scheduledTask,0, Long.parseLong(PropertyUtils.getInstance().getMongoProperty("scheduler.period")));
        } catch (Exception e) {
            LOGGER.error(""+e);
        }
    }

}
