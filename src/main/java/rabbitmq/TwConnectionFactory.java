package rabbitmq;

import com.rabbitmq.client.ConnectionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import utils.PropertyUtils;

/**
 * Created by vinoth on 7/8/16.
 */
public class TwConnectionFactory {
    private static Logger LOGGER= LoggerFactory.getLogger(TwConnectionFactory.class);
    private static ConnectionFactory factory;
    public static ConnectionFactory getRabbitMqConnectionFactory(){
        if(factory == null){
            factory = new ConnectionFactory();
        }
        factory.setHost(PropertyUtils.getInstance().getRMQProperty("rmqHost"));
        factory.setUsername(PropertyUtils.getInstance().getRMQProperty("rmqUserName"));
        // this user is assigned to the below mentioned vhost & exchange
        factory.setPassword(PropertyUtils.getInstance().getRMQProperty("rmqPassWord"));
        factory.setVirtualHost(PropertyUtils.getInstance().getRMQProperty("rmqVhost"));

        LOGGER.info("*** RABBIT MQ THREAD POOL SIZE IS :" + Integer.parseInt(PropertyUtils.getInstance().getRMQProperty("rabbitmq.threadpool.size")));
        System.out.println("*** RABBIT MQ THREAD POOL SIZE IS :"+Integer.parseInt(PropertyUtils.getInstance().getRMQProperty("rabbitmq.threadpool.size")));

        return factory;
    }
}
