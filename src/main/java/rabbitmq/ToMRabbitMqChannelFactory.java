package rabbitmq;

import com.rabbitmq.client.Connection;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

/**
 * Created by vinoth on 4/8/16.
 */
public class ToMRabbitMqChannelFactory {
    private static Logger log= LoggerFactory.getLogger(ToMRabbitMqChannelFactory.class);
    private static Connection mConnection = null;


    public static void start() throws Exception {
        try {
            ToMRbMQAbstractTopicListener.initChannel();
        } catch (IOException e) {
            log.error("Exception while initializingchannel");
        } catch (TimeoutException e) {
            e.printStackTrace();
        }
        ToMRbMQListenersInitializer.getInstance().initTopicListeners();// listens northbound
    }

    private void closeConnection(Connection connection) {
        log.info("Request received for close RabbitMQ connection");
        try {
            if(connection !=null){
                connection.close();
                log.info("Successfully closed connection");
            }
        } catch (IOException e) {
            log.error("While closing RabbitMQ connection raised IOException " + e.getMessage(), e);
        } catch (Exception e) {
            log.error("While closing RabbitMQ connection raised Exception " + e.getMessage(), e);
        }
    }

    public void stop() throws Exception {
        closeConnection(mConnection);
    }
}
