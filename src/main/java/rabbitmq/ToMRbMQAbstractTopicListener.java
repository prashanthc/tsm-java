package rabbitmq;

import com.rabbitmq.client.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import utils.PropertyUtils;

import java.io.IOException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeoutException;

/**
 * Created by vinoth on 4/8/16.
 */
public class ToMRbMQAbstractTopicListener {
    private static Logger LOGGER= LoggerFactory.getLogger(ToMRbMQListenersInitializer.class);
    static Channel channel = null;
    static Connection connection = null;

    public static void initChannel() throws IOException,TimeoutException {
        //ToDO
        ConnectionFactory factory = TwConnectionFactory.getRabbitMqConnectionFactory();
        ExecutorService es = Executors.newFixedThreadPool(Integer.parseInt(PropertyUtils.getInstance().getRMQProperty("rabbitmq.threadpool.size")));
        connection = factory.newConnection(es);
        channel = connection.createChannel();
        channel.exchangeDeclare(PropertyUtils.getInstance().getRMQProperty("Exchange_name"), "topic");

        channel.addShutdownListener(new ShutdownListener() {
            @Override
            public void shutdownCompleted(ShutdownSignalException e) {
                LOGGER.info("shutting down channel, closing thread pool");
                es.shutdown();
            }
        });
    }
}
