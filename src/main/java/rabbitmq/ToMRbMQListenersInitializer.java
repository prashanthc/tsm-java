package rabbitmq;

import consumers.RMQConsumer;
import consumers.RMQConsumerFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import utils.PropertyUtils;

import java.util.ArrayList;
import java.util.Properties;

/**
 * Created by vinoth on 4/8/16.
 */
public class ToMRbMQListenersInitializer {
    private static Logger log= LoggerFactory.getLogger(ToMRbMQListenersInitializer.class);
    private static ToMRbMQListenersInitializer nb_rbMQInitializer = null;
    private PropertyUtils propUtils= PropertyUtils.getInstance();

    // message handler related declarations
    public ArrayList<Object> topiclistenersRefsList = new ArrayList<Object>();
    // this will be primarily used to close down the msg-listeners

    public ArrayList<Object> routesListeningList = new ArrayList<Object>();
    // this is the list of routing_keys we are listening
    public ArrayList<Object> classHandlersForroutesList = new ArrayList<Object>();
    // list of handler classes which are instantiated for every message by msg-rabbitmq.listener

    public static synchronized ToMRbMQListenersInitializer getInstance() {
        if (nb_rbMQInitializer == null) {
            log.debug("loading the classes used for topic listeners towards northbound ");
            nb_rbMQInitializer = new ToMRbMQListenersInitializer();
        } else {
            // nothing
        }
        return nb_rbMQInitializer;
    }// end

    // IMPORTANT -> to add a new routingkey and stats handling class, we need
    // add here, thats pretty wrt rabbitmq stuff
    // THIS IS THE MOST IMPORTANT METHOD IN THIS MODULE,since it initializes all
    // the listeners
    public static void initTopicListeners(){
        boolean autoAckBool = true;
        log.info("Loading RMQ Configurations");
        Properties rmqConsumersProperties = PropertyUtils.getInstance().getRMQConsumersProperties();
        for (String queueName : rmqConsumersProperties.stringPropertyNames()){
            RMQConsumer rmqConsumer = RMQConsumerFactory.getInstance(queueName);
            String autoAck = rmqConsumersProperties.getProperty(queueName);
            if(autoAck!=null && !autoAck.isEmpty() && Boolean.parseBoolean(autoAck)){
                autoAckBool = Boolean.valueOf(autoAck);
            }
            log.info("Queue Name : "+queueName +" Auto Ack :"+autoAck);
            rmqConsumer.consumeMessage(queueName, "*.*." + queueName,autoAckBool);

        }
        // end while loop
    }// end initTopicListeners
}
