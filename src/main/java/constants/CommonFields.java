package constants;

/**
 * Created by vinoth on 10/8/16.
 */
public class CommonFields {
    public static final String HOST_ID = "host_id";
    public static final String CHAIN = "chain";
    public static final String STORE = "store";
    public static final String MSG_TYPE="msg_type";
    public static final String LOST_PLAY_CREDIT_TIME ="lost_playcredit_time";
    public static final String OFFLINE_GT_DAY="offline_gt_day";
}
