package constants;

/**
 * Created by vinoth on 4/8/16.
 */
public class QueueNameConstants {
    public static final String DEVICE="device";
    public static final String TAG_HEART_BEAT="tag_heartbeat";
    public static final String TGS_STATE="TAGs";
    public static final String ALARMS="alarms";
    public static final String ORATION_CREATION="orations";
    public static final String ORATION_LISTENED="listened";
    public static final String MOS="MOS";
    public static final String PACKET_LOSS_SERVER="packetLoss";
    public static final String SUPPORT_COMMANDS= "support_commands";
    public static final String STATIC_ANNOUNCEMENT="static";

    public static final String DEVICE_COUNT = "device_count";


}
